<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Spatie\Permission\PermissionRegistrar;

use App\Models\User;

class PermissionsDemoSeeder extends Seeder
{
    /**
     * Create the initial roles and permissions.
     *
     * @return void
     */
    public function run()
    {
        // Reset cached roles and permissions
        app()[PermissionRegistrar::class]->forgetCachedPermissions();

        // create permissions
        Permission::create(['name' => 'create plan']);
        Permission::create(['name' => 'edit plan']);
        Permission::create(['name' => 'approve plan']);
        

        // create roles and assign existing permissions
        $creator = Role::create(['name' => 'creator']);
        $creator->givePermissionTo(['create plan','edit plan']);
       

        $evaluator = Role::create(['name' => 'evaluator']);
        $evaluator->givePermissionTo('approve plan');

        // assign roles to demo users
    
        $user = \App\Models\User::factory()->create([
            'name' => 'Example User',
            'email' => 'test@example.com',
        ]);

        $user->assignRole($evaluator);
        //$user->givePermissionTo('approve plan');

        $user1 = \App\Models\User::factory()->create([
            'name' => 'User',
            'email' => 'email@example.com',
        ]);

        $user1->assignRole($creator);

    }
}
