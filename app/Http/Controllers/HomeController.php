<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

use App\Models\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        //Role::create(['name'=>'creator']);
        //Permission::create(['name'=>'create plan']);

        //$permission = Permission::create(['name'=>'edit plan']);

        //$role = Role::findById(1);

        //$permission = Permission::findById(1);

        //$role->givePermissionTo($permission);

        //$permission->removeRole($role);

        //$role->revokePermissionTo($permission);

        //auth()->user()->givePermissionTo('create plan');

        //auth()->user()->assignRole('creator');

        //return auth()->user()->permissions;

    /*
        $role = Role::findById(1);

        $permission = Permission::findById(2);

        $role->givePermissionTo($permission);

        auth()->user()->assignRole($role);

        return auth()->user()->roles;
    */

        //return auth()->user()->getAllPermissions();

        //return auth()->user()->revokePermissionTo('create plan');

        //return auth()->user()->removeRole('creator');
        
        //return auth()->user()->permissions;

        //return auth()->user()->getPermissionsViaRoles();

        

        return view('home');
    }
}
